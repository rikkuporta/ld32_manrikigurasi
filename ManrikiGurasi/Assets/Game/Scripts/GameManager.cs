﻿
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public GameObject PrefabAudioManager;
    public GameObject PrefabTileSpawner;
    public GameObject PrefabEnemy;
    public int TilesNeeded;

    public PlayerController _char;
    private OrigamiSword _sword;
    private AudioManager _audio;
    private TileSpawner _tspawn;
    private bool _firstTime = true;

    private const float ENEMY_SPAWN_INTERVAL_START = 20.0f;
    private const float ENEMY_SPAWN_INTERVAL_END = 2.0f;
    private const float ENEMY_SPAWN_INTERVAL_REDUCE = 1.0f;
    [SerializeField] private float m_enemySpawnInterval = ENEMY_SPAWN_INTERVAL_START;

    public static bool GameRunning = true;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    // Use this for initialization
    void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(this);
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public PlayerController Character()
    {
        return _char;
    }

    public OrigamiSword OrigamiSword()
    {
        return _sword;
    }

    public AudioManager AudioManager()
    {
        return _audio;
    }

    public TileSpawner TileSpawner()
    {
        return _tspawn;
    }

    public void RegisterTileSpawner(TileSpawner t)
    {
        _tspawn = t;
    }

    public void RegisterOrigamiSword(OrigamiSword s)
    {
        _sword = s;
        _sword.MaxSwordTileLength = TilesNeeded;
    }

    public void RegisterCharacter(PlayerController c)
    {
        _char = c;
    }

	void Start ()
	{
	    InitializeAudioManager();
        StartCoroutine("CheckForDeathWin");
	    //GenerateLevel();
	    //InitializeTileSpawner();
        //SpawnEnemies();
	}

    void Update()
    {
        if (_firstTime)
        {
            
            _firstTime = false;
        }
    }

    IEnumerator CheckForDeathWin()
    {
        yield return new WaitForSeconds(0.1f);
        if (_sword.CurrentSwordTileLength >= TilesNeeded)
        {
			Debug.Log ("blablablabjlba");
			GameObject ending =(GameObject) GameObject.FindWithTag ("Ending");
			ending.transform.position = new Vector3(-2,17,0);
			GameObject.FindWithTag ("MainCamera").GetComponent<Camera>().orthographicSize = 97.5f;
			GameObject.FindWithTag ("MainCamera").GetComponent<Animator>().SetBool ("end", true);
			GameObject.FindWithTag ("Player").transform.position = new Vector3(2.5f,1.4f,0);
			GameObject.FindWithTag ("Player").GetComponent<PlayerController>().enableInput = false;


        }
        else if (_sword.CurrentSwordTileLength < 1)
        {
            StartCoroutine("RestartGame");
            yield break;
        }

        StartCoroutine("CheckForDeathWin");
    }

    public void ForceDeath()
    {
        StartCoroutine("RestartGame");
    }

    IEnumerator RestartGame()
    {
        _char.DoDie();
        yield return new WaitForSeconds(1.5f);
        StopAllCoroutines();
        Application.LoadLevel(Application.loadedLevel);
        StartCoroutine("CheckForDeathWin");
        yield return null;
    }

    void InitializeTileSpawner()
    {
        _tspawn = Instantiate(PrefabTileSpawner).GetComponent<TileSpawner>();
    }
    void InitializeAudioManager()
    {
        _audio = Instantiate(PrefabAudioManager).GetComponent<AudioManager>();
        DontDestroyOnLoad(_audio);
    }

    void GenerateLevel()
    {
        //Start Level Generator here
    }

    public void SpawnEnemies()
    {
        Debug.Log("Spawn Enemies");
        StartCoroutine(SpawnEnemyLoop());
    }

    private IEnumerator SpawnEnemyLoop()
    {
        while (GameRunning)
        {
            m_enemySpawnInterval -= ENEMY_SPAWN_INTERVAL_REDUCE;
            if (m_enemySpawnInterval < ENEMY_SPAWN_INTERVAL_END) m_enemySpawnInterval = ENEMY_SPAWN_INTERVAL_END;
            yield return new WaitForSeconds(m_enemySpawnInterval);
            Instantiate(PrefabEnemy, Vector3.up * 100, Quaternion.identity);
        }
    }
	
}
