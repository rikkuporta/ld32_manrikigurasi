﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{

    public List<AudioClip> BackgroundLoop;
    public AudioClip FootSteps;
    public AudioClip ManrikiVocal;
    public AudioClip Draw;
    public AudioClip Slash;
    public AudioClip Die;
    public AudioClip Collect;
    private AudioSource _source;


	// Use this for initialization
	void Awake ()
	{
	    _source = GetComponent<AudioSource>();
        _source.clip = ManrikiVocal;
        _source.Play();
	    //StartBackgroundLoop();

	}

    public void StartBackgroundLoop()
    {
        StartCoroutine("PlayBackgroundSound");
    }

    IEnumerator PlayBackgroundSound()
    {
        if (_source.isPlaying)
        {
            _source.Stop();
        }
        _source.clip = BackgroundLoop[0];
        _source.Play();
        yield return new WaitForSeconds(BackgroundLoop[0].length);
        StartCoroutine("GetNextBackgroundLoop");
    }

    IEnumerator GetNextBackgroundLoop()
    {
        var pick = Random.Range(0, BackgroundLoop.Count);
        _source.clip = BackgroundLoop[pick];
        _source.Play();
        yield return new WaitForSeconds(BackgroundLoop[pick].length);
        StartCoroutine("GetNextBackgroundLoop");
    }

}
