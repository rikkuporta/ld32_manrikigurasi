﻿using UnityEngine;
using System.Collections;

public class Attack {
	
	public static void Kick(Rigidbody2D _source) {
		Debug.Log (_source.gameObject.name + " kicks!");
		CharacterController2D cc = _source.GetComponent<CharacterController2D>();
		if (cc != null) {
			cc.AttackZoneActive = true;
		}
	}
	
	public static void Slash(Rigidbody2D _source) {
		Debug.Log (_source.gameObject.name + " slashes!");
		CharacterController2D cc = _source.GetComponent<CharacterController2D>();
		if (cc != null) {
			cc.AttackZoneActive = true;
		}
	}
	
	public static void BowShot(Rigidbody2D _source) {
		Debug.Log (_source.gameObject.name + " shoots!");
		CharacterController2D cc = _source.GetComponent<CharacterController2D>();
		if (cc != null) {
			cc.AttackZoneActive = true;
		}
	}
}
