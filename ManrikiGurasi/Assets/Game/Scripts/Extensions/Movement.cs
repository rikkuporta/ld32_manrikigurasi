﻿using UnityEngine;
using System.Collections;

public static class Movement{
	
	public static float lastXAxis = 0.0f;

	public static void MoveHorizontal(this CharacterController2D _cc, float _axis) {

		float vMove = _cc.Specs.vMove;
		float fSpeed = _cc.Specs.fSpeed;
		float yVelocity = _cc.GetComponent<Rigidbody2D>().velocity.y;
		
		if (_axis != 0.0f) {
			// check if velocity is maximum and axis was not released
            //if (Mathf.Abs(_axis) >= Mathf.Abs(lastXAxis))
            //{
                //_cc.GetComponent<Rigidbody2D>().AddForce(new Vector2((float)_cc.dir * fSpeed, 0.0f));
                _cc.GetComponent<Rigidbody2D>().velocity = new Vector2((float)_cc.dir * vMove * Mathf.Abs(_cc.InputXAxis), yVelocity);
            //}
        }
        else if (_cc.Grounded)
        {
            _cc.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, yVelocity);
        }
        		
//		if (_cc.anim != null) {
//			float animSpeed = (int)((xVelocity / mSpeed) * 10) / 10.0f;
//			_cc.anim.SetFloat("Speed", animSpeed);
//			Debug.Log (animSpeed);
//		}
		
		lastXAxis = _axis;
	}
	
	public static void ClimbLadder(this PlayerController _pc, float _axis) {
		_pc.GetComponent<Rigidbody2D>().isKinematic = true;
		_pc.EnableXAxisInput = false;
		_pc.transform.position += Vector3.up * _axis / 7;
		_pc.anim.SetFloat("VSpeed", Mathf.Abs(_axis));
	}
}
