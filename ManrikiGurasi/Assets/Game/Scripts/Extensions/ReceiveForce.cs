﻿using UnityEngine;
using System.Collections;

public static class ReceiveForce {

	public static IEnumerator Push(this CharacterController2D _cc, Vector2 _force, float _dir) {
		//		Debug.Log (gameObject.name + " is pushed " + _force);
		_cc.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		for (int i = 0; i < 5; i++) {
			_cc.GetComponent<Rigidbody2D>().AddForce(_force * _dir);
			yield return new WaitForFixedUpdate();
		}
	}
}