﻿using UnityEngine;
using System.Collections;

public static class Evade {

	public static void Fallback(this CharacterController2D _cc) {
        Rigidbody2D rigidbody2D = _cc.GetComponent<Rigidbody2D>();
		rigidbody2D.velocity = new Vector2(0f, rigidbody2D.velocity.y);
		rigidbody2D.angularVelocity = 0.0f;

		if(Mathf.Abs(rigidbody2D.velocity.x) < _cc.Specs.vMove){
			
//			Debug.Log (_cc.gameObject.name + " is recieving force of " + new Vector2(-_cc.Specs.eSpeedX * (float)_cc.dir, _cc.Specs.eSpeedY));
			_cc.StartCoroutine(_cc.Push(new Vector2(-_cc.Specs.eSpeedX, _cc.Specs.eSpeedY), -(float) _cc.dir));
		}
	}

    public static void JumpForward(this CharacterController2D _cc)
    {
        Rigidbody2D rigidbody2D = _cc.GetComponent<Rigidbody2D>();
		rigidbody2D.velocity = new Vector2(0f, rigidbody2D.velocity.y);
		rigidbody2D.angularVelocity = 0.0f;
		
		_cc.StartCoroutine(_cc.Push (new Vector2(_cc.Specs.eSpeedX, _cc.Specs.eSpeedY * 1.5f), (float) _cc.dir));
	}
}
