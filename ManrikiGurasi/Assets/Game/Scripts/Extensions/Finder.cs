﻿using UnityEngine;
using System.Linq;

public static class Finder
{
	public static Transform FindInChildren(this Transform _t, string _name)
	{
		return (from x in _t.GetComponentsInChildren<Transform>()
		        where x.gameObject.name == _name
		        select x).First();
	}
}