﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TileSpawner : MonoBehaviour {

    public float QuadChance = 0.49f; //equal tiles of restchance distributed to triangle and splitter
    public List<SwordTile> QuadTiles;
    public List<SwordTile> TriangleTiles;
    public List<SwordTile> SplitterTiles;
    public Transform TileSpawnPositions;
    private List<Transform> _spawnPositions; 
    public int MinimumSpawns = 1;
    public int MaximumSpawns = 3;
    public float DespawnAfterSeconds = 15.0f;
    public bool StartWithFirstOne = true;



	// Use this for initialization
	void Start ()
	{
	    //TileSpawnPositions = GameObject.Find("TileSpawnPositions").GetComponent<Transform>();
        GameManager.Instance.RegisterTileSpawner(this);
        _spawnPositions = GetSpawnPositions(TileSpawnPositions);
        StartCoroutine("SpawnTile");
	}


    List<Transform> GetSpawnPositions(Transform origin)
    {
        return TileSpawnPositions.Cast<Transform>().ToList();
    }

    IEnumerator SpawnTile()
    {
        var numberOfTiles = Random.Range(MinimumSpawns, MaximumSpawns + 1);
        var spawnableTiles = new List<SwordTile>();
        var availablePositions = new List<Transform>();

        Debug.Log("New Spawns!");

        //positions
        for (var i = 0; i < numberOfTiles; i++)
        {
            var pick = -1;

            do
            {
                pick = Random.Range(0, _spawnPositions.Count);

            } while (availablePositions.Contains(_spawnPositions[pick]));

            availablePositions.Add(_spawnPositions[pick]);
        }

        //tiles
        for (var i = 0; i < numberOfTiles; i++)
        {
            var chance = Random.Range(0f, 1f);
            if (chance > QuadChance)
            {
                if (chance > QuadChance + (1 - QuadChance)/2)
                    spawnableTiles.Add(TriangleTiles[Random.Range(0, TriangleTiles.Count)]);
                else
                    spawnableTiles.Add(SplitterTiles[Random.Range(0, SplitterTiles.Count)]);
            }
            else
            {
                spawnableTiles.Add(QuadTiles[Random.Range(0, QuadTiles.Count)]);
            }
        }

        var spawnedTiles = new List<SwordTile>();

        //show them!
        for (var i = 0; i < numberOfTiles; i++)
        {
            var tile = Instantiate(spawnableTiles[i]);
            spawnedTiles.Add(tile);

            if (StartWithFirstOne)
            {
                tile.gameObject.transform.position = _spawnPositions[0].position;
                StartWithFirstOne = false;
                continue;
            }

            tile.gameObject.transform.position = availablePositions[i].position;
        }

        yield return new WaitForSeconds(DespawnAfterSeconds);

        Debug.Log("Remove Spawns!");
        //remove tiles which have not been collected
        for (int i = 0; i < numberOfTiles; i++)
        {
            if (spawnedTiles[i].AttachedAtThisPosition == -1)
                Destroy(spawnedTiles[i].gameObject);
        }

        StartCoroutine("SpawnTile");
    }

    // Update is called once per frame
	void Update () {
	
	}
}
