﻿using UnityEngine;
using System.Collections;

public abstract class SwordTile : MonoBehaviour
{
    public Transform BaseAnker;
    public Transform Sprite;
    public Transform[] ExtensionAnkerSide;
    public bool[] SideIsAssigned;
    public Color Color;
    public float Damage = 1;
    public float SpeedBonus = 1;
    public float HealthBonus = 0;
    public float JumpPowerBonus = 0;
    public Transform ThisTile;
    public bool IsBaseElement = false;
    public bool CheckNow = false;

    private SwordTile _attachtedToThisTile;
    public int AttachedAtThisPosition = -1;

    public void Reset()
    {
        ThisTile.position = new Vector3(0, 0, 0);
        ThisTile.localEulerAngles = new Vector3(0, 0, 0);
        if(_attachtedToThisTile != null)
            _attachtedToThisTile.SideIsAssigned[AttachedAtThisPosition] = false;
        AttachedAtThisPosition = -1;
        _attachtedToThisTile = null;
        IsBaseElement = false;

        var theScale = ThisTile.localScale;
        if (theScale.x < 0)
        {
            theScale.x *= -1;
            ThisTile.localScale = theScale;
        }

        for (var i = 0; i < SideIsAssigned.Length; i++)
            SideIsAssigned [i] = false;
    }

    void Update()
    {
        if (CheckNow)
        {
            CheckNow = false;
            CheckIfCollsion();
        }
    }

    //position 0 is ankerForward!
    public void AttachToAnker(SwordTile anker, int position, bool flip)
    {
        var baseTurn = new Vector3(0, 0, (anker.ExtensionAnkerSide[position].rotation.eulerAngles.z));

        if (flip)
        {
            var theScale = ThisTile.localScale;
            theScale.x *= -1;
            ThisTile.localScale = theScale;
        }

        ThisTile.Rotate(baseTurn);
        ThisTile.position = anker.ExtensionAnkerSide[position].position;

        _attachtedToThisTile = anker;
        AttachedAtThisPosition = position;
        _attachtedToThisTile.SideIsAssigned[AttachedAtThisPosition] = true;
    }

    public void AttachToAnker(Vector3 position)
    {
        _attachtedToThisTile = this;
        AttachedAtThisPosition = 0;
        ThisTile.position = position;
    }

    public void LinkTransform()
    {
        ThisTile = gameObject.transform;
    }

    public bool CheckIfCollsion()
    {
        var r = false;
        var positionA = new Vector2(BaseAnker.position.x + 0.13f, BaseAnker.position.y);

        var obj = Physics2D.OverlapCircle(positionA, 0.06f, LayerMask.GetMask("Tiles"));

        if (obj != null && obj.tag == "Tiles")
        {
            if (obj.GetComponent<SwordTile>().AttachedAtThisPosition != -1)
            {
                r = true;
                Debug.Log("collides with " +obj.name);
            }
        }

        return r;
    }
}


