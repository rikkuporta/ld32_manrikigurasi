﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using System.Linq;


    public class OrigamiSword : MonoBehaviour
    {
        public Transform AttachPoint;
        public Transform SwordPoolTransform;
        public List<SwordTile> SwordTilesPool;
        public List<SwordTile> ArrangedSword;

        public SwordTile QuadTilePrefab;

        public int MaxSwordTileLength = 40;
        public int DeformTreshold = 4; //after this, it may get random
        public int BaseLength = 2;

        public int ChangeToSplittingFormAfter = 20;
        public bool RearrangeAfterPickUp = true;
        public bool StopRearrangeAfterSplittingStarted = true;
        
        private bool LinearProgress = true;
        private bool AlternateGeneration = true;
        public bool ReArrange = false;
        public bool RemoveTile = false;
        private bool AddTile = false;

        public int CurrentSwordTileLength = 0; // = ArrangedSword.Count
        private bool ReArrangeInProgress = false;
        private PlayerController _parent;
        
        // Use this for initialization
        void Start () 
        {
            do
            {
                AddBaseTile();
            } while (CurrentSwordTileLength < BaseLength);

            _parent = transform.parent.transform.parent.transform.parent.GetComponent<PlayerController>();

            SwordPoolTransform = _parent.transform.FindChild("Pool");
            SwordPoolTransform.gameObject.SetActive(false);
        }

        void Update()
        {
            if (CurrentSwordTileLength >= ChangeToSplittingFormAfter)
            {
                LinearProgress = false;
            }
            else
            {
                LinearProgress = true;
            }

            if (!LinearProgress)
            {
                AlternateGeneration = false;
            }
            else
            {
                AlternateGeneration = true;
            }

            if (ReArrange && !ReArrangeInProgress)
            {
                ReArrange = false;
                StartCoroutine("ReArrangeSword");
            }

            if (RemoveTile)
            {
                RemoveTile = false;
                RemoveSwordTile();
            }

            if (AddTile)
            {
                AddTile = false;
                AddSwordTile(RandomSwordTile(), false);
            }

            if (!RearrangeAfterPickUp)
            {
                StopRearrangeAfterSplittingStarted = false;
            }

        }

        IEnumerator ReArrangeSword()
        {
            Debug.Log("Start ReArrange!");
            var _bufferTileLength = CurrentSwordTileLength;
            ReArrangeInProgress = true;
            while (CurrentSwordTileLength > BaseLength)
            {
                RemoveSwordTile();
                yield return null;
            }

            while (CurrentSwordTileLength < _bufferTileLength)
            {
                AddSwordTile(RandomSwordTile(), false);
                yield return null;
            }
            ReArrangeInProgress = false;
        }

        public SwordTile AddToSwordPool(SwordTile tile)
        {
            SwordTilesPool.Add(tile);
            tile.LinkTransform();
            tile.ThisTile.parent = SwordPoolTransform;

            return tile;
        }

        public void IncreaseSwordLength()
        {
            MaxSwordTileLength++;
        }

        public void DecreaseSwordLength()
        {
            MaxSwordTileLength--;
        }

        public SwordTile RandomSwordTile()
        {
            if (SwordTilesPool.Count != 0)
            {
                var index = Random.Range(0, SwordTilesPool.Count);
                return SwordTilesPool[index];
            }
            return null;
        }

        public SwordTile AddSwordTile(SwordTile tile, bool collected)
        {

            if (CurrentSwordTileLength >= MaxSwordTileLength) return null;
            if (tile == null) return null;

            SwordTile extension = null;
            var position = 0;
            var validIndex = new List<int>();

            if (CurrentSwordTileLength == 0)
            {
                return AddBaseTile();
            }

            if (CurrentSwordTileLength < DeformTreshold)
            {
                extension = ArrangedSword[ArrangedSword.Count-1];
                position = 0;
            }
            else
            {
                    extension = LinearProgress ? ArrangedSword[ArrangedSword.Count - 1] : GetNextExtension();

                    for (var i = 0; i < extension.ExtensionAnkerSide.Length; i++)
                    {
                        if (AlternateGeneration &&
                            extension.ExtensionAnkerSide[i].GetComponent<AnkerScript>().Colliding() == false)
                            validIndex.Add(i);
                        if (!AlternateGeneration && extension.SideIsAssigned[i] == false)
                            validIndex.Add(i);
                    }

                    //treshold for generation side to forward ratio is 0.45 : 0.55
                    var chance = Random.Range(0f, 1f);
                    position = (chance < 0.45 && validIndex.Count > 1)
                        ? validIndex[Random.Range(1, validIndex.Count)]
                        : 0;
            }
       
            tile.AttachToAnker(extension, position, collected && GetCharacterRotation() );
            tile.ThisTile.parent = transform;

            SwordTilesPool.Remove(tile);
            ArrangedSword.Add(tile);

            CurrentSwordTileLength++;
            if (RearrangeAfterPickUp && !ReArrangeInProgress)
            {
                if (!(StopRearrangeAfterSplittingStarted && !LinearProgress))
                {
                    StartCoroutine("ReArrangeSword");
                }
                
            }
            return tile;
        }
        public SwordTile RemoveSwordTile()
        {
            SwordTile hold = ArrangedSword[ArrangedSword.Count-1];

            ArrangedSword.Remove(hold);
            AddToSwordPool(hold).Reset();

            CurrentSwordTileLength--;

            return hold;
        }

        public SwordTile AddBaseTile()
        {
            var obj = Instantiate(QuadTilePrefab);
            obj.LinkTransform();
            obj.IsBaseElement = true;
            obj.AttachToAnker(AttachPoint.position);
            AttachPoint.position = obj.ExtensionAnkerSide[0].position;
            obj.ThisTile.parent = transform;

            ArrangedSword.Add(obj);
            CurrentSwordTileLength++;
            return obj;
        }

        private bool GetCharacterRotation() //is true when player is looking left
        {
            return !(_parent.transform.localScale.x >= 0);
        }

        public SwordTile GetNextExtension()
        {
            //Take ALL of ArrangedSword WHERE the swordtiles array SideIsAssigned CONTAINS ANY FALSE 
            List<SwordTile> collection = ArrangedSword.Where(swordTile => swordTile.SideIsAssigned.Any(x => x == false)).ToList();

            //get a random
            int index = Random.Range(0, collection.Count);

            return collection[index];
        }
    }


