﻿using UnityEngine;
using System.Collections;

public class PlayerController : CharacterController2D
{

    public float test;
    public float defaultXScale;
    private bool _firstLoad = true;

    public GameObject SwordPrefab;
    public OrigamiSword OrigamiSword;
    public Transform SwordHandTransform;

    void Start()
    {
        var obj = Instantiate(SwordPrefab) as GameObject;
        obj.transform.parent = SwordHandTransform;
        obj.transform.position = SwordHandTransform.position;

        OrigamiSword = obj.GetComponent<OrigamiSword>();
        GameManager.Instance.RegisterOrigamiSword(OrigamiSword);
        GameManager.Instance.RegisterCharacter(gameObject.GetComponent<PlayerController>());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Tiles")
        {

            var obj = other.gameObject.GetComponent<SwordTile>();

            if (obj.AttachedAtThisPosition == -1)
            {
                GetComponent<AudioSource>().clip = GameManager.Instance.AudioManager().Collect;
                GetComponent<AudioSource>().Play();
                OrigamiSword.AddToSwordPool(obj);
                OrigamiSword.AddSwordTile(obj, true);
            }
        }
    }

    void LateUpdate()
    {
        if (_firstLoad)
        {
            SwordHandTransform.Rotate(new Vector3(0, 0, -135f));
            _firstLoad = false;
        }
    }
    new void Update()
    {
        if (enableInput)
        {
            if (enableXAxisInput)
            {
                InputXAxis = Input.GetAxis("Horizontal");
            }

            if (enableAttackInput)
            {
                if (Input.GetButtonDown("Attack"))
                {
                    TriggerAttack();
                }
            }

            if (enableEvadeInput)
            {
                if (Input.GetButtonDown("Evade"))
                {
                    TriggerEvade();
                }
            }

            if (Input.GetButtonDown("Jump"))
            {
                TriggerJump();
            }
        }

        base.Update();
    }

    

    public new void ExecuteAttack()
    {
        Bounds hitbox = getSwordHitbox();
        Collider2D[] enemyColliders = Physics2D.OverlapAreaAll(hitbox.min, hitbox.max, m_WhatIsEnemy);
        for (int i = 0; i < enemyColliders.Length; i++)
        {
            enemyColliders[i].GetComponent<CharacterController2D>().Hurt(1, Vector2.zero);
            Debug.Log("Hurt " + enemyColliders[i].gameObject.name);
        }
    }

    /// <summary>
    /// Forms a hitbox from the hitboxes of all sword tiles and returns it
    /// </summary>
    /// <returns>The Hitbox formed from all sword tiles</returns>
    private Bounds getSwordHitbox()
    {
        if (OrigamiSword == null || OrigamiSword.AttachPoint == null)
        {
            Debug.LogWarning("OrigamiSword or AttackPoint is null");
            return new Bounds();
        }
        Bounds hitbox = new Bounds(OrigamiSword.AttachPoint.position, Vector3.zero);
        SwordTile[] tiles = OrigamiSword.ArrangedSword.ToArray();
        for (int i = 0; i < tiles.Length; i++ )
        {
            hitbox.Encapsulate(tiles[i].GetComponent<Collider2D>().bounds);
        }
        return hitbox;
    }

    void OnDrawGizmos()
    {
        Bounds hitbox = getSwordHitbox();
        Gizmos.DrawWireCube(hitbox.center, hitbox.extents * 2);
    }

    override protected void TriggerDeath()
    {
        GameManager.Instance.ForceDeath();
        base.TriggerDeath();
    }
}
