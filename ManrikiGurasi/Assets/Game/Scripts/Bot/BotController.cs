﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//TODO skeletons have to look out for allies and row in behind them if they are trying to move to the same position
// in a combat situation this means that the the first in the row will fight and the others wait for their turn
// if the fighter evades back or moves behind the enemy or dies, the next in the row will join the fight

public class BotController : CharacterController2D {

    // CONSTANTS

    private const float SAVE_STEP_DISTANCE = 2.0f;
	
	// DELEGATES
	
	public delegate void PerceptionDelegate(List<Transform> _perceived);
	
	public PerceptionDelegate OnNoticeEnemy;
	public PerceptionDelegate OnEnterCombatZone;
	public PerceptionDelegate OnNoticeAlly;
	
	public delegate void MovedToPositionDelegate();
	
	//TODO make the ATTACK_FRAME_DUR soo that it can be used by any animation

	/// duration of one frame in the attack animation in ms
	private const float ATTACK_FRAME_DUR = 0.04375f;
	
	public float ATTACK_OFFSET = 0.39375f;

    [SerializeField] private float m_TeleportCooldown = 2.5f; 
	
	/// radius of notice zone
	public float noticeZoneRadius = 15.0f;
	
	/// radius of combat zone
	public float combatZoneRadius = 7.5f;
	
	/// the tag which the bot sees as enemy
	public List<string> enemyTags = new List<string>();
	
	/// the tag which the bot sees as ally
	public List<string> allyTags = new List<string>();
	
	/// the time which the bot needs to take an action
	public float combatReactionTime = 0.1f;

	/// the activities which can be performed by bots
	protected enum Activity { Newborn, Idle, Follow, Combat, Retreat, Observe };
	
	/// the behaviors a bot can have
    protected enum Behavior { Aggressive, Passive, Defensive };
	
	/// the activity which is currently performed by this bot 
	[SerializeField] protected Activity m_activity;
	
	/// the behavior which the bot currently is in
    [SerializeField] private Behavior m_behaviour;

    private Transform m_AttackZone;

    private float m_TeleportCooldownTimer = 0.0f;

    /// <summary>
    /// he's a lucky, he's a star ...
    /// </summary>
    private bool m_Luck = false;
    [Range(0.01f, 5.0f)] public float Luckify = 0.1f;

	override protected void OnDrawGizmos() {
		base.OnDrawGizmos();
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(transform.position, noticeZoneRadius);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, combatZoneRadius);
	}
	
	/// initializes the bot and starts the perceive coroutine
	override protected void Awake() {
        base.Awake();

        m_AttackZone = transform.Find("AttackZone");

        if (m_AttackZone == null)
        {
            Debug.LogError("Fatal Error: no AttackZone found in " + gameObject.name);
            Application.Quit();
        }

		// start perception of environment
		StartCoroutine("Perceive");
	}

    void Start()
    {
        SpawnOnPlayerPlatform();
    }

    new void Update()
    {
        m_Luck = Random.Range(0.0f, 5.0f) < Luckify;

        if (m_TeleportCooldownTimer > 0)
        {
            m_TeleportCooldownTimer -= Time.deltaTime;
        }
        else
        {
            m_TeleportCooldownTimer = 0.0f;
        }

        base.Update();
    }
	
	protected void ChangeToIdle() {
//		Debug.Log ("Idling...");
//		Say("Gähn....");
		m_activity = Activity.Idle;
		InputXAxis = 0;
//		StopCoroutine(DoIdleStuff());
		StartCoroutine("DoIdleStuff");
	}

	/// The bot will begin to follow the target.
	protected void ChangeToFollow(Transform _target) {
//		Debug.Log (Time.time + ": Charging at " + _target.gameObject.name + "!");
		m_activity = Activity.Follow;
//		StopCoroutine(Follow());
		StartCoroutine("Follow", _target);
//		Say("Stay still!");
	}
	
	protected void ChangeToCombat(Transform _target) {
//		Debug.Log (Time.time + ": Attacking " + _target.gameObject.name + "!");
		m_activity = Activity.Combat;
//		Say("En Garde! >:D");
		StopCoroutine("Combat");
		StartCoroutine("Combat", _target);
	}
	
	protected void ChangeToRetreat(float _distance) {
		m_activity = Activity.Retreat;
        StopCoroutine("Perceive");
		StartCoroutine(MoveToPosition(transform.position.x + _distance, ChangeToIdle));
	}
	
	public void SendToPosition(float _position, MovedToPositionDelegate _OnArrival = null) {
		StopCoroutine("MoveToPosition");
		StartCoroutine(MoveToPosition(_position, _OnArrival));
	}

    public void ChangeToObserve(Transform _target, int _animHash)
    {
        m_activity = Activity.Observe;
        m_behaviour = Behavior.Passive;
        StartCoroutine(Observe(_target, _animHash));
    }

    /// coroutine for orbserving an animation state on the target and launching a delegate when it changes
    protected IEnumerator Observe(Transform _target, int _animHash)
    {
        while (_target.GetComponent<CharacterController2D>().animationState.fullPathHash == _animHash)
        {
            yield return new WaitForEndOfFrame();
        }
        m_behaviour = Behavior.Aggressive;
        yield return null;
    }
	
	/// coroutine for checking the environment for enemies
	protected IEnumerator Perceive() {
		while (true) {
			List<Transform> noticed = new List<Transform>();
			List<Transform> combatants = new List<Transform>();
			List<Transform> allies = new List<Transform>();
			foreach (string tag in enemyTags) {
				foreach (GameObject enemy in GameObject.FindGameObjectsWithTag(tag)) {
					Vector3 myPos = transform.position;
					Vector3 enemyPos = enemy.transform.position;
					float delta = Vector3.Distance(enemyPos, myPos);
					if (Mathf.Abs(delta) <= noticeZoneRadius) {
						if (Mathf.Abs(delta) <= combatZoneRadius) {
							combatants.Add(enemy.transform);
                        }
                        else
                        {
							noticed.Add(enemy.transform);
						}
					}
				}
			}
			
			foreach (string tag in allyTags) {
				foreach (GameObject enemy in GameObject.FindGameObjectsWithTag(tag)) {
					Vector3 myPos = transform.position;
					Vector3 allyPos = enemy.transform.position;
					float delta = Vector3.Distance(allyPos, myPos);
					if (Mathf.Abs(delta) <= noticeZoneRadius) {
						allies.Add(enemy.transform);
					}
				}
			}
			allies.Remove(transform);
			
			if (combatants.Count == 0) {
                if (noticed.Count == 0)
                {
					// no enemy in the notice zone
//					Debug.Log ("No one noticed...");
					if (m_activity != Activity.Idle) {
						ChangeToIdle();
					}
				} else {
					// there is at least one enemy in the notice zone
					if (m_activity != Activity.Follow) {
						// sort enemies by distance to this transform
						noticed.Sort(new DistanceComparer(transform));
						// charge to the nearest enemy
                        if (m_behaviour != Behavior.Passive)
                        {
                            ChangeToFollow(noticed[0]);
                        }
					}
					if (OnNoticeEnemy != null) {
						OnNoticeEnemy(noticed);
					}
				}
			} else {
				// there is at least one enemy in the combat zone
//				if (combatants.Count > 2) {
					// I am overpowered
					// TODO: check if allies are near, else retreat
//					ChangeToRetreat(25.0f);
//				} else if (m_activity != Activity.Combat) {
					//sort enemies by distance to this transform
					combatants.Sort(new DistanceComparer(transform));
					// get in combat with the nearest
					ChangeToCombat(combatants[0]);
//				}
				if (OnEnterCombatZone != null) {
					OnEnterCombatZone(combatants);
				}
			}
			
			if (OnNoticeAlly != null && allies.Count != 0) {
				OnNoticeAlly(allies);
			}
			// wait for next update frame
			yield return new WaitForEndOfFrame();
		}
	}
	
	protected IEnumerator DoIdleStuff() {
		yield return null;
//				──────█▀▄─▄▀▄─▀█▀─█─█─▀─█▀▄─▄▀▀▀─────
//				──────█─█─█─█──█──█▀█─█─█─█─█─▀█─────
//				──────▀─▀──▀───▀──▀─▀─▀─▀─▀──▀▀──────
//				─────────────────────────────────────
//				───────────────▀█▀─▄▀▄───────────────
//				────────────────█──█─█───────────────
//				────────────────▀───▀────────────────
//				─────────────────────────────────────
//				─────█▀▀▄─█▀▀█───█──█─█▀▀─█▀▀█─█▀▀───
//				─────█──█─█──█───█▀▀█─█▀▀─█▄▄▀─█▀▀───
//				─────▀▀▀──▀▀▀▀───▀──▀─▀▀▀─▀─▀▀─▀▀▀───
//				─────────────────────────────────────
//				─────────▄███████████▄▄──────────────
//				──────▄██▀──────────▀▀██▄────────────
//				────▄█▀────────────────▀██───────────
//				──▄█▀────────────────────▀█▄─────────
//				─█▀──██──────────────██───▀██────────
//				█▀──────────────────────────██───────
//				█──███████████████████───────█───────
//				█────────────────────────────█───────
//				█────────────────────────────█───────
//				█────────────────────────────█───────
//				█────────────────────────────█───────
//				█────────────────────────────█───────
//				█▄───────────────────────────█───────
//				▀█▄─────────────────────────██───────
//				─▀█▄───────────────────────██────────
//				──▀█▄────────────────────▄█▀─────────
//				───▀█▄──────────────────██───────────
//				─────▀█▄──────────────▄█▀────────────
//				───────▀█▄▄▄──────▄▄▄███████▄▄───────
//				────────███████████████───▀██████▄───
//				─────▄███▀▀────────▀███▄──────█─███──
//				───▄███▄─────▄▄▄▄────███────▄▄████▀──
//				─▄███▓▓█─────█▓▓█───████████████▀────
//				─▀▀██▀▀▀▀▀▀▀▀▀▀███████████────█──────
//				────█─▄▄▄▄▄▄▄▄█▀█▓▓─────██────█──────
//				────█─█───────█─█─▓▓────██────█──────
//				────█▄█───────█▄█──▓▓▓▓▓███▄▄▄█──────
//				────────────────────────██──────────
//				────────────────────────██───▄███▄───
//				────────────────────────██─▄██▓▓▓██──
//				───────────────▄██████████─█▓▓▓█▓▓██▄
//				─────────────▄██▀───▀▀███──█▓▓▓██▓▓▓█
//				─▄███████▄──███───▄▄████───██▓▓████▓█
//				▄██▀──▀▀█████████████▀▀─────██▓▓▓▓███
//				██▀─────────██──────────────██▓██▓███
//				██──────────███──────────────█████─██
//				██───────────███──────────────█─██──█
//				██────────────██─────────────────█───
//				██─────────────██────────────────────
//				██─────────────███───────────────────
//				██──────────────███▄▄────────────────
//				███──────────────▀▀███───────────────
//				─███─────────────────────────────────
//				──███────────────────────────────────
	}
	
	protected IEnumerator MoveToPosition(float _destPos, MovedToPositionDelegate _OnArrival) {
		float delta;
	
		do {
			//move
			Vector3 pos = transform.position;
			delta = _destPos - pos.x;
			if (Mathf.Abs(delta) > 0.1f) {
				MoveInDirection(delta);
			}
			yield return new WaitForEndOfFrame();
		} while (Mathf.Abs (delta) > 0.1f);
		InputXAxis = 0;
		if (_OnArrival != null) _OnArrival();
	}
	
	protected IEnumerator Follow(Transform _target) {
		while (m_activity == Activity.Follow) {
			if (_target != null && animationState.fullPathHash != evadeHash && enableXAxisInput) {
				Vector3 myPos = transform.position;
				Vector3 targetPos = _target.position;
				float delta = Vector3.Distance(targetPos, myPos);
                float deltaX = targetPos.x - myPos.x;

				if (delta > combatZoneRadius) {
                    if (_target.GetComponent<CharacterController2D>().Ground == Ground)
                    {
                        MoveInDirection(deltaX);
                    }
                    else if (Ground != null && Grounded &&  m_TeleportCooldownTimer == 0.0f)
                    {
                        InputXAxis = 0;
                        yield return new WaitForSeconds(1.2f);
                        anim.SetTrigger("Teleport");
                        yield return new WaitForSeconds(1.0f);
                        SpawnOnPlayerPlatform();
                    }
				}
			}
			yield return new WaitForEndOfFrame();
		}
	}
	
	/// Combat logic, biggest part of AI
	protected IEnumerator Combat(Transform _target) {
		while (m_activity == Activity.Combat) {			
			if (_target != null) {
				CharacterController2D cc = _target.GetComponent<CharacterController2D>();
				if (cc != null && enemyTags.Contains(cc.gameObject.tag) && cc.animationState.fullPathHash != deathHash && cc.animationState.fullPathHash != deadHash) {
					// actual combat starts here
					Vector3 myPos = transform.position;
					// where is my target at?
					Vector3 targetPosition = _target.position;
					// what is my target doing?
					int targetAnimationHash = cc.animationState.fullPathHash;
					// in which direction is he facing?
					float targetDir = (float)cc.dir;
					// how fast and in which direction is my target moving?
					float targetVelocity = _target.GetComponent<Rigidbody2D>().velocity.x;
					// get distance with direction
					float delta = Vector3.Distance(_target.position, myPos);
                    float deltaX = _target.position.x - myPos.x;
					//wait for reaction time
//					Debug.Log (Time.time + ": Start reaction...");
//					yield return new WaitForSeconds(combatReactionTime);
//					Debug.Log(Time.time + ": React!");

					dir = (ViewDir)Mathf.Sign(deltaX);

                    if (delta > combatZoneRadius)
                    {
                        ChangeToIdle();
                        yield break;
                    }

					// check if player is in range
					if (delta <= classSpecs.attackRadius) {
						// hero in range
						if (targetAnimationHash == attackHash) {
							// hero is  attacking
							if (animationState.fullPathHash != attackHash) {
								// bot is not attacking
								InputXAxis = 0;
                                if (m_Luck)
                                {
                                    TriggerEvade();
                                }
							}
						} else if (animationState.fullPathHash != attackHash) {
							// no one is attacking
							InputXAxis = 0;
                            if (m_Luck)
                            {
                                TriggerAttack();
                            }
						}
					} else if (targetAnimationHash != jumpHash) {
						// hero not in range
						if (aCooldownTimer == 0.0f) {
							// bot has attack ready
							// predict next delta
							float nextAdvance = PredictNextAdvance(deltaX, targetVelocity, 9);
							if (Mathf.Abs(deltaX) - nextAdvance <= classSpecs.attackRadius) {
								// target will be in range when attack is triggered
//								Debug.Log("Triggering early attack!");
								InputXAxis = 0;
                                if (m_Luck)
                                {
                                    TriggerAttack();
                                }
							} else {
								// target is too far away
//								Debug.Log("closing in");
								if (animationState.fullPathHash != attackHash && !inTransition) {
									MoveInDirection(deltaX);
								}
							}
						} else {
							// bot has attack cooldown
							// predict next advance
							float nextAdvance = PredictNextAdvance(deltaX, targetVelocity, 4);
							if (Mathf.Abs(deltaX) - nextAdvance <= classSpecs.attackRadius && targetAnimationHash == attackHash) {
								// hero will be in range but attack is not ready
//								Debug.Log(gameObject.name + " is evading the charge!");
								InputXAxis = 0;
                                if (m_Luck)
                                {
                                    TriggerEvade();
                                }
							} else {
								// target is too far away
//								Debug.Log("closing in");
								if (animationState.fullPathHash != attackHash && !inTransition) {
									MoveInDirection(deltaX);
								}
							}
						}
                    }
                    else 
                    {
                        //ChangeToRetreat(0.4f);
                        ChangeToObserve(_target, targetAnimationHash);
                        yield break;
                    }
				} else {
					ChangeToIdle();
					yield break;
				}
			} else {
				ChangeToIdle();
				yield break;
			}
			
			yield return new WaitForEndOfFrame();
		}
	}
	
	/// Returns the next advance of distance to the target, i.e. the distance which the target will be closer to me after the number of frames, in meters
	private float PredictNextAdvance(float _delta, float _targetVelocity, int _frames = 1) {
//		float myVelocity = rigidbody2D.velocity.x;
		float advance = _targetVelocity * -Mathf.Sign(_delta) * ATTACK_OFFSET * Random.Range(0.5f, 1.5f);
		return advance;
	}

    override protected void TriggerDeath()
    {
        StopAllCoroutines();
        GetComponent<AudioSource>().clip = GameManager.Instance.AudioManager().Die;
        GetComponent<AudioSource>().Play();
		base.TriggerDeath ();
	}

    protected void MoveInDirection(float _dir)
    {
        if (NextStepIsSave(_dir))
        {
            InputXAxis = Mathf.Sign(_dir);
        }
        else
        {
            StopCoroutine("MoveToPosition");
            InputXAxis = 0;
        }
    }

    protected bool NextStepIsSave(float _dir)
    {
        bool save = false;
        if (Ground != null)
        {
            Bounds groundBounds = Ground.GetComponent<Collider2D>().bounds;
            float x = transform.position.x;
            if (_dir < 0)
            {
                if (x > groundBounds.min.x + SAVE_STEP_DISTANCE)
                {
                    save = true;
                }
            }
            else
            {
                if (x < groundBounds.max.x - SAVE_STEP_DISTANCE)
                {
                    save = true;
                }
            }
        }
        return save;
    }

    protected new void ExecuteAttack()
    {
        Collider2D[] enemyColliders = Physics2D.OverlapCircleAll(m_AttackZone.position, 0.5f, m_WhatIsEnemy);
        for (int i = 0; i < enemyColliders.Length; i++)
        {
            enemyColliders[i].GetComponent<CharacterController2D>().Hurt(1, Vector2.zero);
        }
    }

    public void SpawnOnPlayerPlatform()
    {
        Debug.Log("Spawning on Player platform");
        GameObject player = GameObject.FindWithTag("Player");

        if (player != null)
        {
            GameObject ground = player.GetComponent<CharacterController2D>().Ground;
            if (ground == null)
            {
                GameObject[] groundsInScene = GameObject.FindGameObjectsWithTag("Ground");
                ground = groundsInScene[Random.Range(0, groundsInScene.Length - 1)];
            }
            Bounds groundBounds = ground.GetComponent<Collider2D>().bounds;
            float x = Random.Range(groundBounds.center.x, (player.transform.position.x < groundBounds.center.x) ? groundBounds.max.x : groundBounds.min.x);
            float y = groundBounds.max.y + 0.5f;
            transform.position = new Vector3(x, y, 0);
            anim.SetTrigger("Appear");
        }
        else
        {
            Debug.LogWarning("Player not found!");
        }
    }
}