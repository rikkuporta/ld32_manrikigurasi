﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** Compares the distances between transforms and source transform to each other */
public class DistanceComparer : IComparer<Transform> {

	private Transform m_source;
	
	public DistanceComparer(Transform _source) {
		m_source = _source;
	}
	
	public int Compare(Transform _t1, Transform _t2) {
		float distanceT1 = Mathf.Abs(_t1.position.x - m_source.position.x);
		float distanceT2 = Mathf.Abs(_t2.position.x - m_source.position.x);
		
		if (distanceT1 == distanceT2) return 0;
		if (distanceT1 > distanceT2) return 1;
		else return -1;
	}
}
