﻿using UnityEngine;
using System.Collections;

public class AttackZone : MonoBehaviour {
	public float duration = 0.01f;
	
	void Awake() {
		
		duration = 0.01f;
	}
	
	void Start() {
	}
	
	void Update() {
	}
	
	void OnTriggerEnter2D(Collider2D _other) {
		CharacterController2D cc = _other.GetComponent<CharacterController2D>();
		ClassSpecs cs = transform.parent.GetComponent<CharacterController2D>().Specs;
		if (cc != null && cc.transform != transform) {
//			Debug.Log (transform.parent.gameObject.name + " hit " + _other.gameObject.name + " with " + cs.aDamage + " damage.");
			cc.Hurt(cs.aDamage, new Vector2(cs.aForce.x * Mathf.Sign (cc.transform.position.x - transform.parent.position.x), cs.aForce.y));
		}
	}
}
