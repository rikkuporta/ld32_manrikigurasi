﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ClassSpecs), typeof(Animator), typeof(AudioSource))]
public abstract class CharacterController2D : MonoBehaviour {
	
    // CONSTANTS

    private const float WALL_JUMP_COOLDOWN = 0.5f;

	/// viewing direction, Left = -1, Right = 1
	public enum ViewDir { Left = -1, Right = 1 };
	/// current viewing direction
	public ViewDir dir;
	
	
// INPUT FIELDS
	
	/// caches the given Input (can originate from either human or AI controls)
	private float inputAxis;
	
	public float InputXAxis {
		get { return inputAxis; }
		set {
			if (enableXAxisInput) {
				inputAxis = value;
			}
		}
	}
	
	// - enable input fields
	/// enables and disables input from controller
	public bool enableInput = true;
	public bool enableXAxisInput = true;
	public bool enableAttackInput = true;
	public bool enableEvadeInput = true;
	
	public bool EnableXAxisInput { 
		get { return enableXAxisInput; }
		set {
			enableXAxisInput = value;
			if (!value) { // enableXAxis was set to false, so make character stop
				inputAxis = 0.0f;
			}
		}
	}
	
	/// Inverts the input
	public bool inverseControl = false;

// GROUND AND WALL CHECKING FIELDS
    [SerializeField] protected bool m_AirControl = false;                 // Whether or not a player can steer while jumping;
    private bool m_AirControlStart = false;
    [SerializeField] protected LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character
    [SerializeField] protected LayerMask m_WhatIsWall;
    [SerializeField] protected LayerMask m_WhatIsEnemy;

    private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    [SerializeField] protected GameObject m_Ground = null;
    public GameObject Ground {
        get { return m_Ground; }
        set { m_Ground = value; }
    }
    private Transform m_WallCheck;    // A position marking where to check if the player is touching a wall
    const float k_WallCheckRadius = .4f; // Radius of the overlap circle to determine if touching wall
    private bool m_Grounded;            // Whether or not the player is grounded.
    public bool Grounded
    {
        get { return m_Grounded; }
    }
    private bool m_TouchingWall = false;  // IF the player touches the Walls
	
// ANIMATION FIELDS

	public enum CharacterID { Skeleton = 1, Golem = 2, Hero = 3 };
	public CharacterID id;
	
	// - state hashes
	protected int idleHash;
	protected int runHash;
	protected int attackHash;
	protected int evadeHash;
	protected int evadeFwdHash;
	protected int deathHash;
	protected int deadHash;
	protected int ladderHash;
    protected int jumpHash;
    protected int wallslideHash;

    // - bool hashes

    /// hash for the animators grounded bool
    private int groundedBoolHash;
	
	// - trigger hashes
	
	/// hash for the animators attack trigger
	private int attackTriggerHash;
	/// hash for the animators evade trigger
	private int evadeTriggerHash;
	/// hash for the animators evade forward trigger
	private int evadeFwdTriggerHash;

	/// reference to attached Animator
	public Animator anim;
	
	/// getter for current animation state hash, returns the value of GetCurrentAnimatorStateInfo(0)
	public AnimatorStateInfo animationState { get { return anim.GetCurrentAnimatorStateInfo(0); } }
	
	/// states if animation is in transition. is set in Update to enable access from FixedUpdate
	protected bool inTransition;
	
// ATTACK ZONE FIELDS
	
	/// reference to attack zone child component
    public AttackZone attackZone;
	
	/// Getter and setter for the active state of attack zone
	public bool AttackZoneActive { 
		get { return attackZone.gameObject.activeSelf; } 
		set { attackZone.gameObject.SetActive(value); }
	}
	
// TIMER FIELDS
	
	/// timer for attack cooldown
	protected float aCooldownTimer;
	/// timer for hurt cooldown
	protected float hCooldownTimer;
	/// timer for evade cooldown
	protected float eCooldownTimer;
    /// timer for jump cooldown
    protected float jCooldownTimer;
    /// 
    protected float walljumpCooldownTimer = 0f;
	
// SPECS FIELDS
	
	/// basic setup for this class
	protected ClassSpecs classSpecs;
	public ClassSpecs Specs { get { return classSpecs; } }
	
	/// current health of the character
	public float health = 1.0f;

    [SerializeField] protected bool isInvincible = false;

//	protected GameObject speechBox;
	
//	public float speechCooldown = 1.5f;
	//	private float speechTimer;
	
	/// <summary>
	/// Draws gizmos for the attack radius	
	/// </summary>
	protected virtual void OnDrawGizmos() {
		Gizmos.color = Color.magenta;
		Gizmos.DrawWireSphere(transform.position, transform.GetComponent<ClassSpecs>().attackRadius);
	}
	
	protected virtual void Awake() {
		
		// setup cooldown timers
		aCooldownTimer = 0.0f;
		eCooldownTimer = 0.0f;
		hCooldownTimer = 0.0f;
        jCooldownTimer = 0.0f;
		
		// setup components
		if ((classSpecs = GetComponent<ClassSpecs>()) == null) {
			Debug.LogWarning("No class specs on " + gameObject.name);
		}
		
		if ((anim = GetComponent<Animator>()) == null) {
			Debug.LogWarning("No animator on " + gameObject.name);
		}
		
        //if ((attackZone = GetComponentInChildren<AttackZone>()) == null) {
        //    Debug.LogError("No attack zone on " + gameObject.name);
        //} else {
        //    attackZone.gameObject.SetActive(false);
        //}
		
//		speechTimer = speechCooldown;
		
		// setup initial values from classSpecs
		dir = classSpecs.startViewDir;
		health = classSpecs.initHealth;
		
        // setup animation bool hashes

        groundedBoolHash = Animator.StringToHash("Grounded");

		// setup animation trigger hashes
        
		attackTriggerHash = Animator.StringToHash("Attack");
		evadeTriggerHash = Animator.StringToHash("Evade");
		evadeFwdTriggerHash = Animator.StringToHash("Evade_Fwd");
		
		// setup animation state hashes
		idleHash = Animator.StringToHash("Base Layer.Idle");
		runHash = Animator.StringToHash("Base Layer.Run");
		attackHash = Animator.StringToHash("Base Layer.Attack");
		evadeHash = Animator.StringToHash("Base Layer.Evade");
		evadeFwdHash = Animator.StringToHash("Base Layer.Evade_Fwd");
		deathHash = Animator.StringToHash("Base Layer.Death");
		deadHash = Animator.StringToHash("Base Layer.Dead");
		ladderHash = Animator.StringToHash("Base Layer.Ladder");
        jumpHash = Animator.StringToHash("Base Layer.Jumping");
        wallslideHash = Animator.StringToHash("Base Layer.Wallsliding");

		inTransition = false;

        m_GroundCheck = transform.Find("GroundCheck");
        m_WallCheck = transform.Find("WallCheck");

        if (m_GroundCheck == null)
        {
            Debug.LogError("Fatal Error: no GroundCheck found in " + gameObject.name);
            Application.Quit();
        }
        if (m_WallCheck == null)
        {
            Debug.LogError("Fatal Error: no WallCheck found in " + gameObject.name);
            Application.Quit();
        }

        m_AirControlStart = m_AirControl;
	}

    public void DoDie()
    {
        anim.SetTrigger("Die");
    }

    void FixedUpdate()
    {
        bool lastTouchingWall = m_TouchingWall;
        m_TouchingWall = false;
        m_Grounded = false;

        if (walljumpCooldownTimer > 0.0f)
        {
            walljumpCooldownTimer -= Time.fixedDeltaTime;
        }
        else
        {
            walljumpCooldownTimer = 0.0f;
        }

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject && !colliders[i].isTrigger)
            {
                m_Grounded = true;
                m_Ground = colliders[i].gameObject;
            }
        }
        anim.SetBool("Grounded", m_Grounded);

        Collider2D[] wallColliders = Physics2D.OverlapCircleAll(m_WallCheck.position, k_WallCheckRadius, m_WhatIsWall);
        for (int i = 0; i < wallColliders.Length; i++)
        {
            if (wallColliders[i].gameObject != gameObject)
            {
                if (!m_Grounded && !lastTouchingWall)
                {
                    m_AirControl = false;
                }
                m_TouchingWall = true;
            }
        }

        if (!m_TouchingWall)
        {
            m_AirControl = m_AirControlStart;
        }

        if (!m_Grounded && m_TouchingWall && animationState.fullPathHash == wallslideHash)
        {
            Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
            if (rigidbody.velocity.y < 0)
            {
                rigidbody.AddForce(new Vector2(0.0f, classSpecs.fWallSlide));
            }
        }

        float axis = (inverseControl ? -inputAxis : inputAxis);

        if (axis != 0.0f)
        {
            dir = (ViewDir)Mathf.Sign(axis);
        }

        if (m_Grounded || m_AirControl && walljumpCooldownTimer == 0.0f)
        {
            if (animationState.fullPathHash != deathHash && (animationState.fullPathHash != evadeHash || animationState.normalizedTime >= classSpecs.eDur) && !inTransition)
            {
                this.MoveHorizontal(axis);
            }
        }
    }
	
	protected virtual void Update() {
        //float scaleX = Mathf.Lerp(transform.localScale.x, classSpecs.defaultXScale * (float)dir, Time.deltaTime * classSpecs.flipSpeed);
        transform.localScale = new Vector3((float) dir, transform.localScale.y, transform.localScale.z);

        if (animationState.fullPathHash != attackHash)
        {
            if (aCooldownTimer > 0.0f)
            {
                aCooldownTimer -= Time.deltaTime;
            }
            else
            {
                aCooldownTimer = 0.0f;
            }
        }

        // update cooldown timers

        if (hCooldownTimer > 0.0f)
        {
            hCooldownTimer -= Time.deltaTime;
        }
        else
        {
            hCooldownTimer = 0.0f;
        }

        if (eCooldownTimer > 0.0f)
        {
            eCooldownTimer -= Time.deltaTime;
        }
        else
        {
            eCooldownTimer = 0.0f;
        }

        if (jCooldownTimer > 0.0f)
        {
            jCooldownTimer -= Time.deltaTime;
        }
        else
        {
            jCooldownTimer = 0.0f;
        }

        /*if (speechBox.activeSelf) {
            speechTimer -= Time.deltaTime;
            if (speechTimer <= 0.0f) {
                speechBox.SetActive(false);
                speechTimer = speechCooldown;
            }
        }*/

        if (animationState.fullPathHash == deadHash)
        {
            CleanupAndDestroy();
        }

        inTransition = anim.IsInTransition(0);

        if (GetComponent<Rigidbody2D>() != null) anim.SetFloat("Speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x / classSpecs.vMove));

        anim.SetBool("Wallsliding", m_TouchingWall);

        //if (animationState.fullPathHash != deathHash && (animationState.fullPathHash != evadeHash || animationState.normalizedTime >= classSpecs.eDur) && !inTransition)
        //{
        //    this.MoveHorizontal((inverseControl) ? -inputAxis : inputAxis);
        //    Debug.Log(inputAxis);
        //}
	}
	
	public void Hurt(float _damage, Vector2 _force) {
		if (hCooldownTimer == 0.0f && health > 0.0f && !isInvincible) {
			hCooldownTimer = classSpecs.hurtCooldown;
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            StartCoroutine(this.Push(_force, (float) dir));
			health -= _damage;
			if (health <= 0.0f) {
				TriggerDeath();
			}
		}
	}

    protected void TriggerJump()
    {
        if (jCooldownTimer == 0.0f)
        {
            if (m_Grounded)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0.0f, classSpecs.fJump));
                anim.SetBool("Grounded", false);
                jCooldownTimer = classSpecs.jCooldown;
                GetComponent<AudioSource>().clip = GameManager.Instance.AudioManager().FootSteps;
                GetComponent<AudioSource>().Play();
            }
            else if (m_TouchingWall)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(classSpecs.fWallPush * -(float)dir, classSpecs.fWallJump));
                jCooldownTimer = classSpecs.jCooldown;
                //m_AirControl = m_AirControlStart;
                walljumpCooldownTimer = WALL_JUMP_COOLDOWN;
                GetComponent<AudioSource>().clip = GameManager.Instance.AudioManager().FootSteps;
                GetComponent<AudioSource>().Play();
            }
        }
    }

	protected void TriggerAttack() {
		if (aCooldownTimer == 0.0f && enableAttackInput) {
			anim.SetTrigger("Attack");
			aCooldownTimer = classSpecs.aCooldown;
            GetComponent<AudioSource>().clip = GameManager.Instance.AudioManager().Slash;
            GetComponent<AudioSource>().Play();
		}
	}

	protected void TriggerEvade() {
		if (eCooldownTimer == 0.0f
			&& ((animationState.fullPathHash != evadeHash && animationState.fullPathHash != evadeFwdHash) || animationState.normalizedTime >= classSpecs.eDur)
			&& enableEvadeInput) {

			if (inputAxis == 0) {
				anim.SetTrigger("Evade");
                //this.Fallback();
			} else {
				anim.SetTrigger("EvadeForward");
                //this.JumpForward();
			}
			eCooldownTimer = classSpecs.eCooldown;
		}
	}
	
	protected virtual void TriggerDeath() {
		if (animationState.fullPathHash != deathHash) {
            anim.SetTrigger("Die");
			anim.SetFloat("Speed", 0);
            Transform attackZone = transform.Find("AttackZone");
            if (attackZone != null)
            {
                attackZone.gameObject.SetActive(false);
            }
			inputAxis = 0;
			enableInput = false;
			transform.localScale = new Vector3((int)dir, transform.localScale.y, transform.localScale.z);
            CleanupAndDestroy();
		}
	}

    public virtual void ExecuteAttack()
    {
        
    }
	
	public void EnableAttackZone() {
		attackZone.gameObject.SetActive(true);
	}
	
	public void DisableAttackZone() {
		attackZone.gameObject.SetActive(false);
	}
	
	protected virtual void CleanupAndDestroy() {
        Destroy(gameObject, 1.5f);
        return;

		if (gameObject.tag == "Player") {
            //GameMaster.current.Restart();
//			deathPlane.GetComponent<Animator>().Play ("DeathPlane");
            GameManager.Instance.ForceDeath();
		}

		if(gameObject.tag == "Golem"){
            //GameMaster.current.GameOver();
//			deathPlane2.GetComponent<Animator>().Play("DeathPlaneGolem");
		}
		
//		gameObject.tag = "Untagged";
	
		foreach (Collider2D col in GetComponents<Collider2D>()) {
			col.isTrigger = true;
		}
		
		gameObject.tag = "Untagged";
		GetComponent<Rigidbody2D>().isKinematic = true;
		Destroy(this);
		Destroy(classSpecs);
	}

    public void ActivateInvicibility()
    {
        isInvincible = true;
    }

    public void DeactivateInvincibility()
    {
        isInvincible = false;
    }
}