﻿using UnityEngine;
using System.Collections;

public class ClassSpecs : MonoBehaviour {


	/** movement speed */
	public float vMove = 7.0f;

	/** force speed */
	public float fSpeed = 100.0f;

    /// force of jump
    public float fJump = 800.0f;

    /// force of wall jump
    public float fWallJump = 700.0f;

    /// force of wall push
    public float fWallPush = 600.0f;

    /// force of wall slide
    public float fWallSlide = 1.0f;
	
	/** horizontal evade speed */
	public float eSpeedX = 200.0f;
	
	/** vertical evade speed */
	public float eSpeedY = 100.0f;
	
	/** flip speed */
	public float flipSpeed = 10.0f;

// ATTACK

	/** damage */
	public float aDamage = 1.0f;
	/** force */
	public Vector2 aForce = new Vector2(700.0f, 0);
	/** attack cooldown */
	public float aCooldown = 1.2f;
	
	/** time until character can be hurt again after previous hurt */
	public float hurtCooldown = 0.2f;
	
	/** evade cooldown */
    public float eCooldown = 1.0f;

    /** jump cooldown */
    public float jCooldown = 0.1f;
	
	/** evade duration (normalized on evade animation time) 0=no duration, 1=complete animation */
	public float eDur = 1.0f;
	
	/** initial view direction */
	public CharacterController2D.ViewDir startViewDir = CharacterController2D.ViewDir.Right;
	
	/** radius of attack */
	[Range(1.8f,999.0f)]
	public float attackRadius = 1.8f;

	public float initHealth = 1.0f;

	public float defaultXScale;
	public bool dirt;
	
	void Awake() {
		defaultXScale = transform.localScale.x;
	}
	
//	void OnDrawGizmos() {
//		Gizmos.DrawSphere(transform.position, attackRadius);
//	}

	public AudioClip walk;
	public AudioClip attack;
	public AudioClip death;
}
