﻿using UnityEngine;
using System.Collections;

public class AnkerScript : MonoBehaviour {

    private bool IsColliding = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Tiles")
        {
            if(other.GetComponent<SwordTile>().AttachedAtThisPosition != -1)
                IsColliding = true;
        }
        
    }

    public bool Colliding()
    {
        return IsColliding;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Tiles")
        {
            if (other.GetComponent<SwordTile>().AttachedAtThisPosition != -1)
                IsColliding = false;
        }
    }
}
