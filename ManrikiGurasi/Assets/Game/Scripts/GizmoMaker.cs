﻿using UnityEngine;
using System.Collections;

public class GizmoMaker : MonoBehaviour
{

    public UnityEngine.Color Color = Color.red;
    public bool ShowAlways = true;
    public float Radius = 0.1f;
	// Use this for initialization

    void OnDrawGizmosSelected()
    {
        if (ShowAlways) return;
        Gizmos.color = Color;
        Gizmos.DrawWireSphere(transform.position, Radius);
    }

    void OnDrawGizmos()
    {
        if (!ShowAlways) return;
        Gizmos.color = Color;
        Gizmos.DrawWireSphere(transform.position, Radius);
    }
}
