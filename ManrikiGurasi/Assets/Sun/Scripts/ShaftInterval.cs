using UnityEngine;
using System.Collections;

public class ShaftInterval : MonoBehaviour {

	public float timer = 1f;
	// Use this for initialization
	void Start () {
	
		GameObject shaft = (GameObject) Instantiate (Resources.Load ("SunShaft_Tile"), gameObject.transform.position, gameObject.transform.rotation);
		shaft.transform.parent = transform;
		StartCoroutine ("Interval", timer);
	}

	IEnumerator Interval(float timer){

		yield return new WaitForSeconds (timer);
		GameObject shaft = (GameObject) Instantiate (Resources.Load ("SunShaft_Tile"), gameObject.transform.position, gameObject.transform.rotation);
		shaft.transform.parent = transform;
		StartCoroutine ("Interval", timer);
	}
}
