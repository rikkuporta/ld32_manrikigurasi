﻿using UnityEngine;
using System.Collections;

public class PlaceAssets : MonoBehaviour {

	public bool random = false;
	public string path = "";
	// Use this for initialization
	void Start () {
	
		if (Random.Range (0, 101) > 70) {
			StartCoroutine ("PlaceAsset", path);
		}

	}

	IEnumerator PlaceAsset(string p){

		BoxCollider2D placeholder = GetComponent <BoxCollider2D>();
		//Debug.Log (placeholder.size+", "+placeholder.transform.position);
		GameObject asset = (GameObject) Instantiate (Resources.Load(p.ToString ()), new Vector3((placeholder.transform.position.x-placeholder.size.x/2)+Random.Range (0, placeholder.size.x),transform.position.y,0), transform.rotation);

		asset.transform.parent = transform;
		yield return null;
	}
	

}
