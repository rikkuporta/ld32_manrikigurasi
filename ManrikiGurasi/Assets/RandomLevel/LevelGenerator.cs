﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour {

	//LEVEL TILE HAVE TO BE 3000px x 3000px!

	public Vector2 size;
	public int layer = 4;  // wieviele Level Leayer gibt es (Bottom, Mid, Top, Empty)

	private int[,] level;

	private int splittingFactor = 0;
	private int splittingFactor_Bottom = 0;
	private int splittingFactor_Mid = 0;
	private int splittingFactor_Top = 0;
	
	private GameObject background;
	private int randomGameSegment = 7;

	private List<GameObject> Segments = new List<GameObject>(); 
	// Use this for initialization
	void Start () {

		StartCoroutine ("Initialize");
		StartCoroutine ("FillLevelWithSegments");

	
	}


	IEnumerator Initialize(){

		ControlSize ();
		//background = Resources
		level = new int[(int)size.x, (int)size.y];

		//splitting Level (Bottom, Mid, Top, Empty)
		splittingFactor = (int) ( size.y / layer );
		splittingFactor_Bottom = (int) splittingFactor - 1;
		splittingFactor_Top = (int) (size.y - splittingFactor) - 1;

		Debug.Log (splittingFactor);

		Instantiate(Resources.Load ("Sun"), new Vector3 ( 0,0,0), Quaternion.identity);
		GameObject background = (GameObject) Instantiate (Resources.Load ("Background"), new Vector3 (2, 0, 0), Quaternion.identity);
		background.transform.localScale = new Vector3 (size.x, size.y, 0);

		Debug.Log ("initialize finished");
		yield return null;
	}

	IEnumerator FillLevelWithSegments(){

		Debug.Log ("start filling");
		for (int y = 0; y < size.y; y++) {

			for ( int x = 0; x < size.x; x++){

				//Bottom

				if( y <= splittingFactor_Bottom ){

					CreateSegment("Segments/Background/Segment_Bottom_Background",x,y);
					CreateSegment("Segments/Game/GameLayerSegment_"+Random.Range (1,randomGameSegment).ToString (),x,y);
				}

				//Middle
				if( y > splittingFactor_Bottom && y < splittingFactor_Top ){

					CreateSegment ("Segments/Background/Segment_Mid_Background", x,y);
					CreateSegment("Segments/Game/GameLayerSegment_"+Random.Range (1,randomGameSegment).ToString (),x,y);
				}
				//Top

				if( y == splittingFactor_Top ){

					CreateSegment ("Segments/Background/Segment_Top_Background", x,y);
					CreateSegment("Segments/Game/GameLayerSegment_"+Random.Range (1,randomGameSegment).ToString (),x,y);
				}

				//Empty

				if( y > splittingFactor_Top ){

					CreateSegment ("Segments/Background/Segment_Empty_Background", x,y);

				}
			}

		}

		SetLevelPosition ();

		yield return null;
	}

	void SetLevelPosition(){

		gameObject.transform.position = new Vector3 ((gameObject.transform.position.x - size.x / 2)*29, (gameObject.transform.position.y - size.y / 2)*30, 0);

	}

	void CreateSegment(string path, int x, int y){
		Debug.Log ("Segments/"+path.ToString ());
		GameObject temp = (GameObject) Instantiate (Resources.Load(path.ToString ()));
		temp.transform.parent = transform;
		temp.transform.position = new Vector3((temp.transform.localScale.x+29) * x, (temp.transform.localScale.y*30) * y,0);
		
	}

	void ControlSize(){

		if (size.x < 4) {
			if (size.y < 4) {
				size = new Vector2 (4f, 4f);
			} else {
				size = new Vector2 (4f, size.y);
			}
		} else {
			if(size.y < 4){
				size = new Vector2 ( size.x, 4);
			}
		}

	}
	// Update is called once per frame
	void Update () {
	
	}
}
