﻿using UnityEngine;
using System.Collections;

public class CameraZoomControl : MonoBehaviour {

	public GameObject title, sub, space;

	public bool start = false;
	public bool zoomEnd = false;

	void Start(){

		GameObject.FindWithTag ("Player").GetComponent<PlayerController>().enableInput = false;
	}

	void Update(){

		if (Input.GetKeyDown (KeyCode.Space) && !start) {

			start = true;
			transform.GetComponent<Animator> ().SetBool ("startZoom", true);
			title.transform.GetComponent<Animator>().SetBool ("start", true);
			sub.transform.GetComponent<Animator>().SetBool ("start", true);
			space.transform.GetComponent<Animator>().SetBool ("start", true);

            GameManager.Instance.AudioManager().StartBackgroundLoop();
            GameManager.Instance.SpawnEnemies();

		}


	}

	void zoomStopped(){

		GameObject.FindWithTag ("Player").GetComponent<PlayerController>().enableInput = true;
		zoomEnd = true;
	}

	/*void StartZoom(){
		
		transform.GetComponent<Animator> ().SetBool ("startZoom", true);
	}

	void SetZoom(bool start){

		transform.GetComponent<Animator> ().SetBool("startZoom", start);
	}*/

}
