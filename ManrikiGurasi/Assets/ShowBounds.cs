﻿using UnityEngine;
using System.Collections;

public class ShowBounds : MonoBehaviour {

	void OnDrawGizmos()
    {
        Gizmos.DrawCube(GetComponent<Collider2D>().bounds.center, GetComponent<Collider2D>().bounds.extents * 2);
    }
}
